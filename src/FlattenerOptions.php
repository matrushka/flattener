<?php

namespace Pilcrum;
use \Exception as Exception;

class FlattenerOptions {
  const VALUE_IS_OPTIONAL = '::';
  const VALUE_IS_REQUIRED = ':';
  const VALUE_IS_IGNORED = '';

  private $options;
  private $available_options = array(
    'e' => ['execute', self::VALUE_IS_OPTIONAL],
    // TODO: review available options, what is target supposed to mean here?
    // 't' => ['target', self::VALUE_IS_OPTIONAL],
    'c' => ['config', self::VALUE_IS_OPTIONAL],
    'h' => ['help', self::VALUE_IS_OPTIONAL],
    'y' => ['yes', self::VALUE_IS_OPTIONAL],
    'd' => ['debug', self::VALUE_IS_OPTIONAL],
    'g' => ['group', self::VALUE_IS_OPTIONAL],
  );

  function __construct() {
    $this->parseOptions();
  }

  function parseOptions() {
    $this->options = getopt($this->getShortOptions(), $this->getLongOptions());
    $this->mapShortOptions();
    $this->setHelpCommand();
    $this->validateOptions();
  }
  
  // Copies short options given to their long counterparts
  function mapShortOptions() {
    foreach($this->available_options as $short => $option) {
      if(array_key_exists($short, $this->options)) {

        // If both the short and the long version are given, throw and Exception
        if(array_key_exists($option[0], $this->options)) {
          throw new Exception('Got ambiguous options. Use only the short or long version of an option, not both: ' . $option[0]);
        }

        // Otherwise, copy the short option value to the long option, and destroy the short one
        $this->options[$option[0]] = $this->options[$short];
        unset($this->options[$short]);
      }
    }
  }

  function validateOptions() {
    foreach($this->available_options as $short => $o) {
      if($o[1] == self::VALUE_IS_REQUIRED && !array_key_exists($o[0], $this->options)) {
        throw new Exception('Missing a required option: ' . $o[0]);
      }
    }
  }

  function setHelpCommand() {
    if(array_key_exists('help', $this->options)) {
      $this->options['execute'] = 'help';
    }
  }

  function getShortOptions() {
    $opts = '';
    foreach($this->available_options as $short => $o) {
      $opts .= $short . $o[1];
    }
    return $opts;
  }

  function getLongOptions() {
    $opts = array_values(array_map(function($o) {
      return $o[0] . $o[1];
    }, $this->available_options));
    return $opts;
  }

  function getOption($option_long_name) {
    if(array_key_exists($option_long_name, $this->options)) {
      return $this->options[$option_long_name];
    }
    else {
      return NULL;
    }
  }

  // Find out if the $options contain certain key(s)
  function hasOptions($option_names) {
    if(is_string($option_names)) {
      return array_key_exists($option_names, $this->options);
    }
    else if(is_array($option_names)) {
      foreach($option_names as $o) {
        if(array_key_exists($o, $this->options)) {
          return true;
        }
      }
      return false;
    }
  }

  function inspect() {
    print_r("These are the contents of options:\n");
    print_r($this->options);
  }
}
