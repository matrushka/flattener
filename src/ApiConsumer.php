<?php

namespace Pilcrum;

trait ApiConsumer {
  function getBaseUrl() {
    $api_config = $this->getApiConfig();
    $a = $api_config;
    $url = "{$a['protocol']}://{$a['host']}:{$a['port']}{$a['prefix']}";

    // Add a trailing slash if missing
    $url = (substr($url, -1) != '/') ? $url . '/' : $url;
    return $url;
  }

  function getApiConfig() {
    $downloader_config = $this->getDownloaderConfig();
    $api_id = $downloader_config['api'];
    $apis_config = $this->config->getValue('apis');
    return $apis_config[$api_id];
  }

  function getDownloaderConfig() {
    $downloaders_config = $this->config->getValue('downloaders');
    return $downloaders_config[$this->id];
  }
  
  /**
    * Makes a GET request to the API's endpoint, without additional parameters
    */
  function simpleApiCall() {
    $url = $this->getBaseUrl() . $this->getDownloaderConfig()['endpoint'];
    $max_retries = $this->config->getValue('http_client')['max_retries'];

    $response = $this->httpGet($url, $max_retries); 

    return $response->raw_body;
  }

  /**
    * Performs a GET request using Httpful
    * @return \Httpful\Response
    * @param $url String
    * @param $max_retries Integer
    */
  function httpGet($url, $max_retries) {
    $num_retries = 1;

    while(TRUE) {
      try {
        $request = \Httpful\Request::get($url);
        $response = $request->send();

        if(method_exists($this, 'validateHTTPResponse')) {
          $this->validateHttpResponse($response);
        }

        return $response;

        break;
      }
      catch(\Pilcrum\APIResponseFatalException $e) {
        print "Attempt #$num_retries of $max_retries on $url.\n";
        print "Got a fatal API Exception: " . $e->getMessage() . "\n";
        print "The operation will NOT be retried.\n";

        return '';
        break;
      }
      catch(\Exception $e) {
        print "Attempt #$num_retries out of $max_retries on $url.\n";
        print "Got HTTP Error: " . $e->getMessage() . "\n";
        $num_retries++;

        if($num_retries == $max_retries) {
          throw new Exception('Too many failed HTTP retries');
          return false;
        }
        else {
          print "The operation will be retried.\n";
        }
      }
    }
  }
}
