<?php

namespace Pilcrum;

trait StorageUser {
  function getStoragePath() {
    return $this->config->getValue('storage') . '/';
  }

  function prepareStorage($file_path = FALSE) {
    // Prepare the general storage directory
    $dir = $this->getStoragePath();
    if(!is_dir($dir)) {
      mkdir($dir, 0755, TRUE);
    }

    // If a specific file path was received, prepare its container
    if($file_path) {
      $path_parts = explode('/', $file_path);
      $filename = array_pop($path_parts);
      $dir_path = join('/', $path_parts);
      if(!is_dir($dir_path)) {
        mkdir($dir_path, 0755, TRUE);
      }
    }
  }

  function saveFile($file_path, $contents) {
    $this->prepareStorage($file_path);


    file_put_contents($file_path, $contents);
  }

  function getOutputFileName() {
    $downloader_config = $this->getDownloaderConfig();
    if(array_key_exists('output_file', $downloader_config)) {
      $file_name = $this->getStoragePath() . $downloader_config['output_file'] . '.json';
    }
    else {
      $file_name = $this->getStoragePath() . $downloader_config['endpoint'] . '.json';
    }
    
    print_r('>> Outputting to: ' . $file_name . "\n");
    return $file_name;
  }
}
