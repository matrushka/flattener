<?php

namespace Pilcrum;

class ExampleDownloader {
  private $config;
  private $options;
  
  /**
    * Constructor
    * 
    * @param FlattenerConfig $config The configuration object
    * @see FlattenerConfig class
    * @param FlattenerOptions $options The command-line options object
    * @see FlattenerOptions class
    */
  function __construct($config, $options) {
    $this->config = $config;
    $this->options = $options;
  }
  
  /**
    * Executes the get hook
    */
  function get() {
    // Perform any API requests and store the results
    // As an example, we make a call to jsontest.com
    $jsontest_api = $this->config->getValue('api_jsontest'); 

    // Build the URL from the API configuration 
    // TODO: turn API configuration and calls into a class
    // TODO: consider using something like http://phphttpclient.com
    $url = $jsontest_api['protocol'] . '://' . $jsontest_api['host'] . $jsontest_api['prefix'] . '/' . $jsontest_api['endpoints']['index'];

    // Make a call to the API
    $api_result = file_get_contents($url);

    // Save the output file

    // TODO: turn this into a storage class that prepares directores, handles permissions, etc
    $output_path = $this->config->getValue('storage');
    if(!is_dir($output_path)) {
      mkdir($output_path, 0775, TRUE);
    }

    $output_file = $output_path . '/' . 'example.time.json'; 
    file_put_contents($output_file, $api_result);
  }

  function clean() {
    $output_path = $this->config->getValue('storage');
    $output_file = $output_path . '/' . 'example.time.json'; 
    unlink($output_file);
  }

  function help() {
    print_r("This is an example implementation of the help hook, in the ExampleDownloader class \n");
  }
}
