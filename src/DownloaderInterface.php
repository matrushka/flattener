<?php

namespace Pilcrum;

Interface DownloaderInterface {
  /**
    * Should retrieve data from an API and store it locally
    */
  public function get();

  /**
    * Should remove all / any files it created while running
    */
  public function clean();
}
