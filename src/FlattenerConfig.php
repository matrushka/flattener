<?php

namespace Pilcrum;
use \Exception as Exception;
use Symfony\Component\Yaml\Yaml;

class FlattenerConfig {
  private $defaults_file = __DIR__ . '/../default.config.yml';
  private $overrides_file = 'config';

  private $defaults;
  private $overrides;
  private $config;

  function __construct($config_file) {
    try {
      if(!empty($config_file)) {
        $this->overrides_file = $config_file;
      }

      $this->loadDefaults();
      $this->loadOverrides();
      $this->mergeConfig();
    }
    catch (Exception $e) {
      echo "Caught exception. " . $e->getMessage() . "\n";
    }
  }

  function loadDefaults() {
    $file_contents = file_get_contents($this->defaults_file);
    $this->defaults = Yaml::parse($file_contents);
  }

  function loadOverrides() {
    $file_name = $this->getOverridesFileName();
    if(!file_exists($file_name)) {
      $this->overrides = array();
      return;
    }

    $file_contents = file_get_contents($file_name);
    $this->overrides = Yaml::parse($file_contents);
  }

  function getOverridesFileName() {
    $file_name = $this->overrides_file . '.yml';
    return $file_name;
  }

  function mergeConfig() {
    $this->config = $this->defaults;
    foreach($this->overrides as $key => $value) {
      $this->config[$key] = $value;
    }
  }

  function getValue($key) {
    if(array_key_exists($key, $this->config)) {
      return $this->config[$key];
    }
    else {
      throw new Exception('Attempted to get undefined configuration value with key: ' . $key);
    }
  }

  function inspect() {
    print_r("Loaded overrides file:\n");
    print_r($this->getOverridesFileName()."\n");
    print_r("These are the contents of the configuration:\n");
    print_r($this->config);
  }
}
