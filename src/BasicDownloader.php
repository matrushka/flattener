<?php

namespace Pilcrum;

class BasicDownloader implements DownloaderInterface {
  use ApiConsumer;
  use StorageUser;

  protected $config;
  protected $id;

  /**
    * @param FlattenerConfig $config The whole Flattener config
    * @param String $id the key of the downloader configuration to be instanced
    */ 
  function __construct($config, $id) {
    $this->config = $config;
    $this->id = $id;
  }
  
  /**
    * Makes a call to an API and saves the result in a single JSON file
    */
  function get() {
    $result = $this->simpleApiCall();
    $this->saveFile($this->getOutputFileName(), $result);
  }
  
  /**
    * Removes the JSON file created during get(), if it exists
    */
  function clean() {
    print_r("this will be the downloader clean(); \n");
  }
}
