<?php

namespace Pilcrum;
use \Exception as Exception;
require 'vendor/autoload.php';

/**
  * This class coordinates the downloading of data, files, their processing and storage
  * It doesn't do anything by itself, it just triggers the actions of the registered
  * downloading routines
  */
class Flattener {
  private $config;
  private $options;
  private $downloaders;
  private $debug;

  function __construct() {
    try {
      $this->options = new FlattenerOptions();
      $this->debug = !is_null($this->options->getOption('debug'));
      $this->config = new FlattenerConfig($this->options->getOption('config'));
      $this->initDownloaders();
      
      $this->debugSetup();
      
      $this->autoExecute();
    }
    catch (Exception $e) {
      echo "Caught exception. " . $e->getMessage() . "\n";
    }
  }

  private function debugSetup() {
    if($this->debug) {
      $this->options->inspect();
      $this->config->inspect();
    }
  }

  private function initDownloaders() {
    $this->downloaders = array();
    $downloaders_configuration = $this->config->getValue('downloaders');

    if(!empty($downloaders_configuration)) {
      foreach($downloaders_configuration as $id => $downloader) {
        $this->downloaders[$id] = $this->instanceDownloader($id);
      }
    }
  }

  private function instanceDownloader($id) {
    $class = $this->config->getValue('downloaders')[$id]['class'];
    return new $class($this->config, $id);
  }

  private function autoExecute() {
    $command = $this->options->getOption('execute');

    if(empty($command)) {
      return;
    }

    if($command == 'help') {
      $this->executeHelp();
    }

    else {
      $this->executeCommand($command);
    }
  }

  private function executeHelp() {
    $specific_help = $this->options->getOption('help');

    if(!empty($specific_help)) {
      $this->downloaders[$specific_help]->help();
    }
    else {
      $this->help();
    }
  }
  
  public function executeCommand($command) {
    $this->commandHelp($command);
    if(!$this->confirmExecution($command)) return;

    // TODO: document the "group" option
    // TODO: document the "named_groups" configuration variable
    // Logic: if the "group" option is given (string), then
    // only process the downloaders mentioned in that named group,
    // that should be defined in config (otherwise FlattenerConfig will
    // throw an error)
    if($this->options->hasOptions('group')) {
      try {
        $group_id = $this->options->getOption('group');
        $groups = $this->config->getValue('named_groups');

        if(!array_key_exists($group_id, $groups)) {
          throw new Exception('Attempted to execute command $command on undefined group: ' . $group_id);
        }
        else {
          $group_members = $groups[$group_id];
        }
      }
      catch (Exception $e) {
        echo "Caught exception. " . $e->getMessage() . "\n";
        echo "Make sure that named groups are configured before trying to use them. \n";
      }
    }

    foreach($this->downloaders as $downloader_id => $downloader) {
      $use_group = isset($group_members);
      
      // $in_group is set to true if groups are not in use (the ALL group is implied,
      // and therefore all downloaders are $in_group,
      // or if the downloader_id is actually included in the current group
      $in_group = !$use_group || ($use_group && in_array($downloader_id, $group_members));

      if(method_exists($downloader, $command) && $in_group) {
        print_r(" >> Will execute $command on $downloader_id\n");
        $downloader->$command();
      }
    }
  }

  private function confirmExecution($command) {
    // If the -y or --yes flags were passed, don't confirm, just keep running
    if($this->options->hasOptions('yes')) {
      return true;
    }

    $confirm = readline("** Please confirm that you want to run the command " . $command . ' [y/N]: ');

    $run = ($confirm === 'y' || $confirm === 'yes');

    if(!$run) {
      print_r(">> Okay, stopping right here. \n");
    }
    else {
      print_r(">> [sound of downloader engines starting]\n");
    }

    return $run;
  }
  
  // TODO: Improve the help structure and document how it's provided.
  private function commandHelp($command) {
    $help_file = __DIR__ . "/..//help/commands/$command.txt";
    if(file_exists($help_file)) {
      print_r(file_get_contents($help_file));
    }
  }

  private function help() {
    print_r("This is the general help for Flattener.\n");
  }
}
