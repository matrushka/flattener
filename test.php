#!/usr/local/bin/php

<?php

require 'vendor/autoload.php';

$Flattener = new Pilcrum\Flattener();

// A command can be executed from the implementation script
// Calling executeCommand here makes passing the --execute option
// unnecessary
$Flattener->executeCommand('get');
