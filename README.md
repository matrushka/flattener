# Pilcrum Flattener

This library is a helper to ease the downloading of API data, in preparation for static site generators or SPAs. Its purpose is to articulate a repeatable, well documented method to download data, additional files and doing post-processing. It provides flexible downloader configuration and invocation mechanisms.

* For a basic, example downloader command implementation, see example.php
* For an example Downloader class, see ExampleDownloader.inc

v0.0.12
